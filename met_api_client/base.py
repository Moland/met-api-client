from datetime import datetime, timedelta

import pytz
import requests


# Base class to be implemented in API integrations, for making any HTTP requests.
class RequestsAPI:
    def __init__(self, base_url, **kwargs):
        self.base_url = base_url
        self.session = requests.Session()
        for arg in kwargs:
            if isinstance(kwargs[arg], dict):
                kwargs[arg] = self.__deep_merge(getattr(self.session, arg), kwargs[arg])
            setattr(self.session, arg, kwargs[arg])

    def head(self, url, **kwargs):
        return self.session.head(self.base_url + url, **kwargs)

    def get(self, url, **kwargs):
        return self.session.get(self.base_url + url, **kwargs)

    def post(self, url, **kwargs):
        return self.session.post(self.base_url + url, **kwargs)

    def put(self, url, **kwargs):
        return self.session.put(self.base_url + url, **kwargs)

    def patch(self, url, **kwargs):
        return self.session.patch(self.base_url + url, **kwargs)

    def delete(self, url, **kwargs):
        return self.session.delete(self.base_url + url, **kwargs)

    @staticmethod
    def __deep_merge(source, destination):
        for key, value in source.items():
            if isinstance(value, dict):
                node = destination.setdefault(key, {})
                RequestsAPI.__deep_merge(value, node)
            else:
                destination[key] = value

        return destination

# Base class for Met.no API. Further implementations should be a subclass of this class for any of the services listed in their docs(api.met.no).
class MetWeatherAPI(RequestsAPI):
    def __init__(self):
        super(MetWeatherAPI, self).__init__('https://api.met.no/weatherapi/')

    def make_request(self, url, **kwargs):
        r = self.get(url, **kwargs)

        if r.status_code != 200:
            r.raise_for_status()

        return r

    @staticmethod
    def get_time(delta_hours=None, delta_days=None):
        now = datetime.utcnow()
        #tz = pytz.timezone("Europe/Oslo")
        #now = tz.localize(now)
        #print(tz.localize(now))
        #print(now.astimezone(tz))
        #localized = now.astimezone(tz)
        #fmt = '%Y-%m-%d %H:%M:%S %Z%z'
        #print(localized.strftime(fmt))

        if (delta_hours is not None) and (delta_days is not None):
            raise ValueError('Cannot set both "delta_days" and "delta_hours" at the same time')

        if delta_hours is not None:
            date = now + timedelta(hours=delta_hours)
            day = date.day
            hour = date.hour + 1
            delta = date.hour

            if hour == 24:
                hour = 0
                day = day + 1
        elif delta_days is not None:
            date = now + timedelta(days=delta_days)
            day = date.day
            hour = date.hour + 1
            delta = date.hour
        else:
            day = now.day
            #hour = (now + timedelta(hours=1)).hour
            hour = now.hour + 1
            delta = now.hour

        # for i in [day, hour, delta]:
        #     if i < 10:
        #         i = '0{}'.format(i)

        if day < 10:
            day = '0{}'.format(day)

        if hour < 10:
            hour = '0{}'.format(hour)

        if delta < 10:
            delta = '0{}'.format(delta)

        return str(hour), str(day), str(delta)