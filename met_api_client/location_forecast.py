from .base import MetWeatherAPI
from ._variables import LOCATION_FORECAST

from datetime import date, time


class LocationForecast(MetWeatherAPI):
    def __init__(self, lat, lng, msl=None):
        super(LocationForecast, self).__init__()

        self.url = 'locationforecast/1.9/.json'
        self.set_lat_lng(lat, lng)
        self.set_msl(msl)

    def __fetch_data(self):
        if self.msl is not None:
            resource = '{}?lat={}&lon={}&msl={}'.format(self.url,
                                                        self.lat,
                                                        self.lng,
                                                        self.msl)
        else:
            resource = '{}?lat={}&lon={}'.format(self.url,
                                                 self.lat,
                                                 self.lng)

        self.response = self.make_request(resource)

        self.__prepare_data()

    def __prepare_data(self):
        data_json = self.response.json()

        product_string = 'product'
        time_string = 'time'

        if product_string in data_json:
            if time_string in data_json[product_string]:
                self.forecast = data_json[product_string][time_string]
            else:
                raise KeyError('Cannot find key "{}" in data. Has the API been updated recently?'.format(time_string))
        else:
            raise KeyError('Cannot find key "{}" in data. Has the API been updated recently?'.format(product_string))

    # Takes in the result of self.get_time()
    def __filter_forecast(self, hour, day, delta):
        for event in self.forecast:
            if (event['to'][8:10] == day) and (event['to'][11:13] == hour) and (event['to'] == event['from']):
                weather_data = self.__pick_weather_data(event)
            if (event['to'][8:10] == day) and (event['to'][11:13] == hour) and (event['from'][11:13] == delta):
                weather_symbol_and_rain = self.__pick_weather_symbol_and_rain(event)

        return {**weather_data, **weather_symbol_and_rain}

    @staticmethod
    def __pick_weather_data(forecast):
        return dict(
            time = forecast['to'],
            temperature = dict(
                value = forecast['location']['temperature']['value'],
                unit = forecast['location']['temperature']['unit']
            ),
            humidity = dict(
                value = forecast['location']['humidity']['value'],
                unit = forecast['location']['humidity']['unit']
            ),
            wind = dict(
                direction = dict(
                    degrees = forecast['location']['windDirection']['deg'],
                    name = forecast['location']['windDirection']['name']
                ),
                speed = dict(
                    value = forecast['location']['windSpeed']['mps'],
                    name = forecast['location']['windSpeed']['name']
                )
            )
        )

    @staticmethod
    def __pick_weather_symbol_and_rain(forecast):
        return dict(
            symbol = forecast['location']['symbol']['number'],
            precipication = dict(
                value = forecast['location']['precipitation']['value'],
                unit = forecast['location']['precipitation']['unit']
            )
        )

    def get_lat_lng(self):
        return self.lat, self.lng

    def set_lat_lng(self, lat, lng):
        self.lat = lat
        self.lng = lng
        # Set forecast to None so the program is forced to do another fetch.
        self.forecast = None

    def get_msl(self):
        # Make sure there exists an entry for msl
        if self.msl is None:
            return 'No value for has been set for "msl". Try using LocationForecast.set_msl() and pass in the desired msl as the only argument.'

        return self.msl

    def set_msl(self, msl):
        if (type(msl) is not int) and (msl is not None):
            raise TypeError('Wrong type for parameter "msl". Type: "{}".'.format(type(msl)))
        self.msl = msl

    def get_accept_header(self):
        return self.accept_header

    def set_accept_header(self, accept_header):
        if accept_header not in LOCATION_FORECAST["VALID_ACCEPT_HEADERS"]:
            raise ValueError(
                'Invalid value for Accept header: accept_header="{}". Value should be "xml" or "json".'
                .format(accept_header))
        self.accept_header = accept_header

    def get_current(self):
        if self.forecast is None:
            self.__fetch_data()

        # Get a formatted timeset consisting of args: hour, day, delta
        return self.__filter_forecast(*self.get_time())

    def get_n_hours(self, timespan=1):
        '''
        TODO:
        This function should take in a timespan, in hours, to a maximum of 12(?).
        '''
        if self.forecast is None:
            self.__fetch_data()

        if timespan > 24:
            raise ValueError(
                'Value for "timespan" should not be higher than 24. Was set to {}.'
                .format(timespan))

        # Get a formatted timeset consisting of args: hour, day, delta(with delta having a set value of arg: timespan)
        return self.__filter_forecast(*self.get_time(delta_hours=timespan))

    def get_next_midnight(self):
        hour, day, delta = self.get_time()
        delta_midnight = 24 - int(hour)

        return self.get_n_hours(timespan=delta_midnight)

    def get_n_days(self, forecast):
        '''
        TODO:
        This function should take in the parsed forecast as the only argument, then output
        a new list containing the picked data for the next n-hours. The days should include
        forecasts in the range of 0, 22, 3.
        '''
        pass