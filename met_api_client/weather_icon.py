from .base import MetWeatherAPI
from ._variables import WEATHER_ICON


class WeatherIcon(MetWeatherAPI):
    def __init__(self):
        super(WeatherIcon, self).__init__()
        self.url = 'weathericon/1.1/'

    def __call__(self, *args, **kwargs):
        self.get_icon(*args, **kwargs)

    def get_icon(self, symbol, is_night=False, is_polarnight=False, content_type='image/png'):
        if content_type not in WEATHER_ICON.VALID_CONTENT_TYPE:
            raise ValueError(
                'Invalid value for Content-Type: content_type="{}". Value should be either {}'
                .format(content_type, WEATHER_ICON.VALID_CONTENT_TYPE))

        if int(symbol) not in WEATHER_ICON.VALID_SYMBOL_NUMBER:
            raise ValueError(
                'Cannot find weather icon number "{}". Value should be either {}'
                .format(symbol, WEATHER_ICON.VALID_SYMBOL_NUMBER))

        if is_night and is_polarnight:
            raise ValueError('Cannot set both is_night={} and is_polarnight={}.'.format(is_night, is_polarnight))

        resource = '{}?content_type={}&is_night={}&is_polarnight={}&symbol={}'.format(
            self.url,
            'image/png' if 'png' in content_type else 'image/svg%2Bxml',
            '1' if is_night else '0',
            '1' if is_polarnight else '0',
            symbol
        )

        return self.make_request(resource)

