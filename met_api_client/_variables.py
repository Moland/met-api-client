class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

LOCATION_FORECAST = AttrDict({
    'VALID_ACCEPT_HEADERS': [
        'xml',
        'text/xml',
        'json',
        'application/json'
    ]
})

WEATHER_ICON = AttrDict({
    'VALID_CONTENT_TYPE': [
        'png',
        'image/png',
        'svg',
        'image/svg',
        'image/svg+xml'
    ],

    'VALID_SYMBOL_NUMBER': [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        34,
        40,
        41,
        42,
        43,
        44,
        45,
        46,
        47,
        48,
        49,
        50
    ]
})