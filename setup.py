#-*- coding: utf-8 -*-
# import os

from setuptools import setup

# module_path = os.path.join(os.path.dirname(__file__), 'foobar_utils.py')
# version_line = [line for line in open(module_path)
#                 if line.startswith('__version__')][0]
#
# __version__ = version_line.split('__version__ = ')[-1][1:][:-2]

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='met_api_client',
      version='0.0.1',
      # version=__version__,
      description='API client for making calls to api.met.no',
      long_description=readme(),
      keywords='weather client met met.no api.met.no forecast',
      url='https://gitlab.com/Moland/met-api-client',
      author='Bjørn Christian Moland',
      author_email='christianmoland@gmail.com',
      packages=['met_api_client'],
      install_requires=[
          'requests',
          'pytz',
      ],
      test_suite='nose.collector',
      tests_require=['nose'],
      classifiers=[
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Operating System :: OS Independent',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
      ],
      include_package_data=True,
      zip_safe=False)
