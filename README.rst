Met.no API Client
-----------------

Requirements:

* >= Python 3.5

* Pip for Python 3

Installation::

    $ git clone https://gitlab.com/Moland/met-api-client.git
    $ cd met-api-client
    $ python setup.py install --user

Basic usage::

    from met_api_client import LocationForecast

    latitude = '59'  # Can be string or int
    longitude = '10'  # Can be string or int

    location_forecast = LocationForecast(latitude, longitude)

    ######

    # The following example returns the current weather forecast for the given coordinates.

    current_forecast = location_forecast.get_current()
    print(current_forecast)

    ######

    # The following example returns the weather forecast for n-hours into the future.
    # It is possible to pass a keyword argument 'timespan' to the function, which is the amount of hours.
    # Default is 1 hours ahead.

    n_hours_forecast = location_forecast.get_n_hours()
    print(n_hours_forecast)

    n_hours_forecast = location_forecast.get_n_hours(timespan=2)
    print(n_hours_forecast)

    n_hours_forecast = location_forecast.get_n_hours(timespan=4)
    print(n_hours_forecast)

    ######

Development:

*It is recommended to install the package in an isolated environment.*
*To do so, run the following commands before installing the package:*
::

    $ cd <directory for installing the package>
    $ pip install virtualenv
    $ virtualenv -p python3.x venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

To run tests(WIP)::

    python setup.py test