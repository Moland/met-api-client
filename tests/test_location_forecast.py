from unittest import TestCase

from met_api_client import LocationForecast


LATITUDE = '59'
LONGITUDE = '10'


class TestLocationForecast(TestCase):
    location_forecast = LocationForecast(LATITUDE, LONGITUDE)

    def test_get_lat_lng(self):
        lat, lng = self.location_forecast.get_lat_lng()
        self.assertEqual(lat, LATITUDE)
        self.assertEqual(lat, self.location_forecast.lat)
        self.assertEqual(lng, LONGITUDE)
        self.assertEqual(lng, self.location_forecast.lng)
        self.assertIsInstance(lat, str)
        self.assertIsInstance(lng, str)

    def test_set_lat_lng(self):
        latitude, longitude = '58', '9'
        self.location_forecast.set_lat_lng(latitude, longitude)
        lat, lng = self.location_forecast.get_lat_lng()
        self.assertEqual(lat, latitude)
        self.assertEqual(lat, self.location_forecast.lat)
        self.assertEqual(lng, longitude)
        self.assertEqual(lng, self.location_forecast.lng)
        self.assertIsNone(self.location_forecast.forecast)

    def test_get_msl(self):
        pass

    def test_set_msl(self):
        pass

    def test_get_accept_header(self):
        pass

    def test_set_accept_header(self):
        pass

    def test_get_current(self):
        pass

    def test_get_n_hours(self):
        pass

    def test_get_n_days(self):
        pass