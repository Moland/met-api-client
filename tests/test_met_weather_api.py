from unittest import TestCase

from met_api_client import MetWeatherAPI


class TestMetWeatherAPI(TestCase):
    met_weather_api = MetWeatherAPI()

    def test_make_request(self):
        pass

    def test_get_time(self):
        for delta_hours in range(6, 19, 6):
            day, hour, delta = self.met_weather_api.get_time(delta_hours=delta_hours)
            self.assertIsInstance(day, str)
            self.assertIsInstance(hour, str)
            self.assertIsInstance(delta, str)

            if int(day) < 10:
                self.assertRegex(day, '0[0-9]')
            if int(hour) < 10:
                self.assertRegex(hour, '0[0-9]')
            if int(delta) < 10:
                self.assertRegex(delta, '0[0-9]')

        for delta_days in range(1, 6):
            day, hour, delta = self.met_weather_api.get_time(delta_days=delta_days)
            self.assertIsInstance(day, str)
            self.assertIsInstance(hour, str)
            self.assertIsInstance(delta, str)

            if int(day) < 10:
                self.assertRegex(day, '0[0-9]')
            if int(hour) < 10:
                self.assertRegex(hour, '0[0-9]')
            if int(delta) < 10:
                self.assertRegex(delta, '0[0-9]')

        with self.assertRaises(ValueError):
            self.met_weather_api.get_time(delta_hours=1, delta_days=1)