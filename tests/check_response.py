def try_connection(forecast_object):
    # lf = LocationForecast('59', '10')
    lf = forecast_object('59', '10')
    cf = lf.get_current()
    next_hour = lf.get_n_hours()
    after_next_hour = lf.get_n_hours(timespan=2)
    next_midnight = lf.get_next_midnight()

    print(cf)
    print(next_hour)
    print(after_next_hour)
    print('########## DEV ZONE ##########')
    print(next_midnight)